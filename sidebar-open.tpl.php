<?php if ($search_box && $first): ?><div class="module-search module">
  <h2 class="module-header">Search</h2>
  <div class="module-content">
    <?php print $search_box ?>
  </div>
</div><?php endif; ?>